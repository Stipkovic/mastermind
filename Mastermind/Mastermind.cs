using System;
using System.IO;
using System.Text;

namespace Mastermind
{
  public class Mastermind
  {
    private const int CodeLength = 4;
    private static readonly char[] codeLatters = { 'A', 'B', 'C', 'D', 'E', 'F' };
    public static void Main(string[] args)
    {
      new Mastermind().play();
    }

    public void play()
    {
      Validator validator = new Validator(CreateCode());

      Intro();

      try
      {
        for (int i = 0; i < 8; i++)
        {
          Console.WriteLine("Guess {0}: ", i + 1);
          string guess = Console.ReadLine();
          string feedback = validator.Validate(guess);

          if (feedback.Equals(Validator.CorrectGuess))
          {
            Win();
            Console.ReadLine();
            return;
          }
          else
          {
            ConsoleColor defaultColor = Console.BackgroundColor;
            for (int j = 0; j < Math.Min(guess.Length, CodeLength); j++)
            {
              Console.BackgroundColor = feedback[j].Equals(Validator.Green) ? ConsoleColor.Green : ConsoleColor.Yellow;
              Console.Write(guess[j]);
            }
            Console.BackgroundColor = defaultColor;
            Console.WriteLine();
          }
        }

        Lose();
      }
      catch (IOException e)
      {
        TextWriter errorWriter = Console.Error;
        errorWriter.WriteLine(e.Message);
      }
    }

    private static string CreateCode()
    {
      StringBuilder codeBuilder = new StringBuilder();
      Random random = new Random();

      for (int i = 0; i < Mastermind.CodeLength; i++)
      {
        codeBuilder.Append(Mastermind.codeLatters[random.Next(Mastermind.codeLatters.Length)]);
      }

      return codeBuilder.ToString();
    }

    private void Intro()
    {
      Console.WriteLine("Welcome to");
      Console.WriteLine("    __  ___           __                      _           __");
      Console.WriteLine("   /  |/  /___ ______/ /____  _________ ___  (_)___  ____/ /");
      Console.WriteLine("  / /|_/ / __ `/ ___/ __/ _ \\/ ___/ __ `__ \\/ / __ \\/ __  /");
      Console.WriteLine(" / /  / / /_/ (__  ) /_/  __/ /  / / / / / / / / / / /_/ /");
      Console.WriteLine("/_/  /_/\\__,_/____/\\__/\\___/_/  /_/ /_/ /_/_/_/ /_/\\__,_/");
      Console.WriteLine("The computer has created a four-letter code from letters A-F (e.g. 'EDEA')");
      Console.WriteLine("You have eight guesses to crack it.");
      Console.WriteLine("After each guess you will receive feedback in the form of colored circles ({0} or {0})\n\n", Validator.Yellow, Validator.Green);
      Console.WriteLine("Example code: ADAE\nYour guess: BEAF\nFeedback: {0}{1} (A: right character, right position, E: right character, wrong position)\n", Validator.Green, Validator.Yellow);
    }

    private void Win()
    {
      Console.WriteLine("Congratulations! You cracked the code!");
    }

    private void Lose()
    {
      Console.WriteLine("You lost! What a pity. Maybe give it another try...");
    }

  }
}
